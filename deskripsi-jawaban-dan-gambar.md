# 1. Apa Itu DevOps dan gambarkan flow Devops

## Definisi DevOps
Kata Devops Gabungan dari Development dan Operations, Metodologi pengembangan Software yang menggabungkan antara Software (Dev) dan Infrastruktur System (Ops).
Devops sangat penting bagi perusahaan IT karena Mempersiapkan Infrastruktur System di mulai dari Server, Deployment produk dari tim Developer, monitoring server dan dapat melakukan inovasi lebih cepat bagi produknya juga dapat rollback dengan mudah jika terjadi bug yang tidak ke cover oleh tim QA.

## flow DevOps

![Images info](./folder-images-jawaban/gambar-jawaban-01.png)

* Devops mempersiapkan Reporsitory Untuk developer Berkerja
* Setelah Developer selesai, Devops melakukan proses build code dari source code developer
* Melakukan Test code oleh QA setelah lolos selanjutnya di deploy oleh Devops ke server.
* Terakhir Devops melakukan Monitoring di Server.

# 2. Jelaskan apa itu Ansible dan berikan kelebihan serta kekurangan menggunakan Ansible?

## Ansibel
Ansible adalah tool automation yang dimiliki oleh perusahaan yang bergerak dibidang teknologi yaitu RedHat. Ansible adalah perangkat lunak open source yang mengotomatiskan pemasangan software, manajemen konfigurasi, dan beberapa tugas lainnya. Ansible terhubung dengan client remotenya menggunakan ssh.

# 3. Command linux yang dapat digunakan untuk mencari sebuah file dalam sebuah directory?
 mencari file menggunakan command find

![Images info](./folder-images-jawaban/03-find.png)

# 4. Untuk mengetahui list command linux yang pernah digunakan sebelumnya, dapat menggunakan perintah?
untuk mengetahui list command yang sudah di gunakan silahkan menggunakan command history

![Images info](./folder-images-jawaban/04-history.png)

# 5. Definisikan database SQL dan NoSQL menurut Kamu, serta berikan contoh dari masing-masing database yang telah Kamu sebutkan?

## Database sql
Structured Query Language (SQL) adalah sistem manajemen database relasional (RDBMS) yang dirancang untuk aplikasi dengan arsitektur client/server. Istilah client, server, dan client/server dapat digunakan untuk merujuk kepada konsep yang sangat umum atau hal yang spesifik dari perangkat keras atau perangkat lunak.

### contoh aplikasi sql
* mariadb
* mysql
* postgresql
* microsoft sql server

## Database nosql
Database noSQL adalah Database yang tidak memiliki perintah SQL dan konsep penyimpanannya semistuktural atau tidak struktural dan tidak harus memiliki relasi layaknya tabel-tabel MySQL.

## Contoh aplikasi nosql
* mongodb
* cassandra
* redis

# 6. Install aplikasi web server di komputer kamu, kemudian install php di web server tersebut (dilarang menggunakan aplikasi seperti xampp, lampp).

1. install nginx dan php

    apt get install nginx php-fpm dan php-mysql

![Images info](./folder-images-jawaban/06-install-nginx-php.png)

2. ubah cgi_pathinfo=0

    nano /etc/php/7.4/fpm/php.ini

![Images info](./folder-images-jawaban/06-phpini.png)

3. restart php-fpm

    systemctl restart php7.4-fpm

![Images info](./folder-images-jawaban/06-restart-fpm.png)

4. tambahkan location php di /etc/nginx/sites-available/default

    nano /etc/nginx/sites-available/default

![Images info](./folder-images-jawaban/06-config-nginx.png)

![Images info](./folder-images-jawaban/06-config-nginx2.png)

5. restart nginx

    systemctl restart nginx

![Images info](./folder-images-jawaban/06-restart-nginx.png)

6. membuat file info.php

    nano /var/www/html/info.php

![Images info](./folder-images-jawaban/06-infophp.png)



7. test di web browser

![Images info](./folder-images-jawaban/06-test.png)

# 7. Deploy sebuah project yang telah Kami sediakan ke Heroku menggunakan heroku-cli 

1. install heroku cli

![Images info](./folder-images-jawaban/07-install-heroku-cli.png)

2. clone reporsitory

![Images info](./folder-images-jawaban/07-clone-repo.png)

3. membuat reporsitory baru untuk deploy ke heroku dan pindahkan isi dari repo lama ke repo baru

![Images info](./folder-images-jawaban/07-add-repo-baru.png)

4. git add . untuk simpan ke reporsitory baru

![Images info](./folder-images-jawaban/07-git-add.png)

5. login ke heroku cli

![Images info](./folder-images-jawaban/07-login-heroku.png)

6. membuat repo heroku dan add option --buildpack untuk nodejs static, lalu langkah terakhir push ke heroku dan tunngu sampai proses deploy selesai

![Images info](./folder-images-jawaban/07-push-build.png)

7. test di browser 

![Images info](./folder-images-jawaban/07-test.png)
